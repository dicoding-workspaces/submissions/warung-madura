# Menjadi Front-End Web Developer Expert

Kelas ini terdiri dari 6 (enam) modul atau pembahasan pokok. Selama mengikuti kelas ini beberapa modul mengharuskan Anda berlatih membuat proyek Web Application, hingga pada modul akhir Anda berhasil membuat Web Application yang memiliki responsibilitas, aksesibilitas, performa, dan kemampuan native dengan baik. Selain itu Anda juga akan belajar prinsip Clean Code pada JavaScript dan belajar banyak mengenai testing. 

| **Proyek** | **Link** |
| --- | --- |
| Proyek Pertama, Katalog Restoran | Akses [disini](https://gitlab.com/dicoding-workspaces/submissions/foodex/-/tree/proyek-awal?ref_type=heads) |
| Proyek Kedua, Katalog Restoran + PWA | Akses [disini](https://gitlab.com/dicoding-workspaces/submissions/foodex/-/tree/proyek-kedua?ref_type=heads) |
| Proyek Ketiga, Katalog Restoran PWA + Testing and Optimized | Akses [disini](https://gitlab.com/dicoding-workspaces/submissions/foodex/-/tree/proyek-akhir?ref_type=heads) |
